import React from 'react'
import {Movie} from "./Movie";

export class MovieList extends React.Component {
    render() {
        return (
            <>
                <div className="movie-list-wrapper">
                {(this.props.list.length > 0)
                    ?
                    this.props.list.map((elem) =>
                        <Movie key={elem.id} id={elem.id} title={elem.title} poster={elem.poster} year={elem.year}/>
                    )
                    :
                    <h5>Nothing to show...</h5>
                }
                </div>
            </>
        );
    }
}
