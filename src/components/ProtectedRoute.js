import React from 'react'
import axios from 'axios';
import Cookies from 'universal-cookie';

export class ProtectedRoute extends React.Component {
    render()
    {
        return (
            <>
                <h1>Login page</h1>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>Username</label>
                        <input name="username" className="form-control" onChange={this.handleInputChange}/>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input name="password" type="password" className="form-control" onChange={this.handleInputChange}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Login</button>
                </form>
            </>
        )
    }

    async handleSubmit(event) {
        event.preventDefault();

        const data = {
            username: this.state.username,
            password: this.state.password
        };

        try {
            const response = await axios.post("https://api.vwm-imdb.ml/login", data);
            const apiKey = response.data["user"]["apiKey"];

            const cookies = new Cookies();
            cookies.set('session', apiKey, { path: '/'});
            this.props.history.push('/');
        } catch (error) {
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }
            console.log(error);
        }
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
}