import React from 'react'
import {Link} from "react-router-dom";

export class Movie extends React.Component {
    render() {
        return (
            <div className="movie-item">
                <h3 className = "center"><Link to={`/movie/${this.props.id}`}> {this.props.title} </Link></h3>
                <h4 className="center">({this.props.year})</h4>
                <Link to={`/movie/${this.props.id}`}><img className="poster center" src={this.props.poster} alt={this.props.title}/></Link>
            </div>
        )
    }
}
