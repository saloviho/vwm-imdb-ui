import React from 'react'
import axios from 'axios';
import {Link} from "react-router-dom";
import Cookies from 'universal-cookie';
import {DirectorList} from "./DirectorList";

export class Directors extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {directors: []};
    }

    async componentDidMount()
    {
        await this.parseQueryParams();
    }

    async parseQueryParams()
    {
        let param = {};
        const queryString = this.props.location.search;
        queryString.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
            param[key] = value;
        });

        await this.getDirectors(param["top"], param["offset"], param["name"]);
    }

    render() {
        if(this.state.directors && this.state.params)
        {
            const data = this.state.params;
            const prev = (parseInt(data.offset) - parseInt(data.top) < 0) ? 0 : parseInt(data.offset) - parseInt(data.top);
            const next = parseInt(data.offset) + parseInt(data.top);
            return (
                <>
                    <DirectorList list={this.state.directors}/>
                    <Link to="#" className="btn btn-primary page-button" onClick={() => this.getDirectors(data.top, prev, data.name)}>Prev</Link>
                    <Link to="#" className="btn btn-primary page-button mb-20" onClick={() => this.getDirectors(data.top, next, data.name)}>Next</Link>
                </>
            );
        }else{
            return <></>;
        }
    }


    async getDirectors(top, offset, name)
    {
        const cookies = new Cookies();
        const token = cookies.get("session");

        const param = {top: (top ? top : 10), offset: (offset ? offset : 0), name: name};
        const url = `https://api.vwm-imdb.ml/director?top=${param["top"]}&offset=${param["offset"]}` + (name ?`&name=${name}`:'');
        const response = await axios.get(url, {
            headers: { Authorization: `Basic ${token}`}
        });

        const directorList = response.data["directors"];
        this.setState({directors: directorList, params: param});
        window.scrollTo(0, 0);
    }
}
