import React from 'react'
import axios from "axios";
import {MovieList} from "./MovieList";
import Cookies from "universal-cookie";

export class Liked extends React.Component {
    constructor()
    {
        super();
        this.state = {movies: []};
    }

    render() {
        if(this.state.movies)
        {
            return (
                <>
                    <MovieList list={this.state.movies}/>
                </>
            );
        }else{
            return <></>
        }
    }

    async componentDidMount () {
        await this.getLiked();
    }

    async getLiked()
    {
        const cookies = new Cookies();
        const token = cookies.get('session');

        const response = await axios.get("https://api.vwm-imdb.ml/movie/liked", {
            headers: { Authorization: `Basic ${token}`}
        });

        const movieList = response.data["likedMovies"];
        this.setState({movies: movieList});
    }
}