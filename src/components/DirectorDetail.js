import React from 'react'
import axios from "axios";
import {MovieList} from "./MovieList";
export class DirectorDetail extends React.Component {
    //state = {movie: undefined};
    constructor()
    {
        super();
        this.state = {director: undefined, movies: []};
    }

    render() {
        if(this.state.director && this.state.movies)
        {
            return (
                <>
                    <h1>{this.state.director.name}</h1>
                    <MovieList list={this.state.movies}/>
                </>
            )
        }

        return <></>;
    }


    componentDidMount () {
        const { id } = this.props.match.params
        this.getDirectorDetails(id);
        this.getDirectorMovies(id);
    }

    async getDirectorDetails(id)
    {
        const response = await axios.get(`https://api.vwm-imdb.ml/director/${id}`, {
            headers: { Authorization: "Basic 48b4b865-fbb5-448d-a5fb-56e4fd9ce819"}
        });
        const directorData = response.data["director"];
        this.setState({director: directorData});
    }

    async getDirectorMovies(id)
    {
        const response = await axios.get(`https://api.vwm-imdb.ml/director/${id}/movie`, {
            headers: { Authorization: "Basic 48b4b865-fbb5-448d-a5fb-56e4fd9ce819"}
        });
        const moviesData = response.data["movies"];
        this.setState({movies: moviesData});
    }
}