import React from 'react'
import {Link} from "react-router-dom";

export class Director extends React.Component {
    render() {
        return (
            <div className="movie-item">
                <h3 className = "center"><Link to={`/director/${this.props.id}`}> {this.props.name} </Link></h3>
            </div>
        )
    }
}
