import React from 'react'
import axios from "axios";
import {MovieList} from "./MovieList";
import Cookies from "universal-cookie";
import {Link} from "react-router-dom";

export class Recommend extends React.Component {
    constructor()
    {
        super();
        this.state = {movies: [], limit: 10};
    }

    render() {
        if(this.state.movies && this.state.limit)
        {
            const more = parseInt(this.state.limit) + 10;
            return (
                <>
                    <MovieList list={this.state.movies}/>
                    <Link className="btn btn-primary page-button mb-20" to="#" onClick={() => this.getRecommendations(more)}>More</Link>
                </>
            );
        }else{
            return <></>
        }
    }

    async componentDidMount () {
        await this.parseQueryParams();
    }

    async parseQueryParams()
    {
        let param = {};
        const queryString = this.props.location.search;
        queryString.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
            param[key] = value;
        });

        await this.getRecommendations(param["limit"]);
    }


    async getRecommendations(limit)
    {
        const newLimit = (limit ? limit: 10);

        const cookies = new Cookies();
        const token = cookies.get("session");

        const response = await axios.get(`https://api.vwm-imdb.ml/recommend?limit=${newLimit}`, {
            headers: { Authorization: `Basic ${token}`}
        });

        const movieList = response.data["movies"];
        this.setState({movies: movieList, limit: newLimit});
    }
}