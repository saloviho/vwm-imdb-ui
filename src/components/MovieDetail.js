import React from 'react'
import axios from "axios";
import {Link} from "react-router-dom";
import Cookies from "universal-cookie";
export class MovieDetail extends React.Component {
    constructor()
    {
        super();
        this.state = {directors: [], movie: undefined, liked: false};
    }

    render() {
        if(this.state.movie && this.state.directors)
        {
            const like = this.state.liked ? "Unlike 💔" : "Like ❤️";
            const runtime = this.state.movie.runtime ? this.state.movie.runtime : "N/A";
            const language = this.state.movie.language ? this.state.movie.language : "N/A";
            const year = this.state.movie.year ? this.state.movie.year : "N/A";
            const country = this.state.movie.country ? this.state.movie.country : "N/A";
            const plot =  this.state.movie.plot ? this.state.movie.plot : "";
            const rated = this.state.movie.rated ? this.state.movie.rated : "N/A";
            const actors = this.state.movie.actors ? this.state.movie.actors.join(', ') : "N/A";

            return (
                <>
                    <div className="movie-item-full">
                        <h1>{this.state.movie.title}</h1>
                        <div className="movie-info mb-20">
                            <img className="poster image" src={this.state.movie.poster} alt={this.state.movie.title}/>
                            <div>
                                <table className="table table-striped">
                                    <tbody>
                                    <tr>
                                        <td>Year</td>
                                        <td>{year}</td>
                                    </tr>
                                    <tr>
                                        <td>Runtime</td>
                                        <td>{runtime}</td>
                                    </tr>
                                    <tr>
                                        <td>Rated</td>
                                        <td>{rated}</td>
                                    </tr>
                                    <tr>
                                        <td>Country</td>
                                        <td>{country}</td>
                                    </tr>
                                    <tr>
                                        <td>Language</td>
                                        <td>{language}</td>
                                    </tr>
                                    <tr>
                                        <td>Actors</td>
                                        <td>{actors}</td>
                                    </tr>
                                    <tr>
                                        <td>Director</td>
                                        <td>{this.state.directors.map(e => <><Link key={e.id} to={`/director/${e.id}`}>{e.name}</Link>{'   '}</>)}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <button type="button" className="btn btn-primary w-100" onClick={() => {this.like(this.state.movie.id)}}>{like}</button>
                            </div>
                        </div>
                        <p>{plot}</p>
                    </div>
                </>
            )
        }

        return <></>;
    }


    async componentDidMount () {
        const { id } = this.props.match.params;
        await this.getMovieDetails(id);
        await this.getMovieDirector(id);
        await this.getLikeState(id);
    }

    async like(id)
    {
        const cookies = new Cookies();
        const token = cookies.get('session');

        const action = (this.state.liked) ? "unlike" : "like";
        await axios.post(`https://api.vwm-imdb.ml/movie/${id}/${action}`, {}, {
            headers: { Authorization: `Basic ${token}`}
        });

        await this.getLikeState(id);
    }

    async getMovieDetails(id)
    {
        const cookies = new Cookies();
        const token = cookies.get('session');

        const response = await axios.get(`https://api.vwm-imdb.ml/movie/${id}`, {
            headers: { Authorization: `Basic ${token}`}
        });
        const movieData = response.data["movie"];
        this.setState({movie: movieData});
    }

    async getMovieDirector(id)
    {
        const cookies = new Cookies();
        const token = cookies.get('session');

        const response = await axios.get(`https://api.vwm-imdb.ml/movie/${id}/director`, {
            headers: { Authorization: `Basic ${token}`}
        });
        const directorsData = response.data["directors"];
        this.setState({directors: directorsData});
    }


    async getLikeState(id)
    {
        const cookies = new Cookies();
        const token = cookies.get('session');

        const response = await axios.get("https://api.vwm-imdb.ml/movie/liked", {
            headers: { Authorization: `Basic ${token}`}
        });

        const likedMovies = response.data["likedMovies"];
        const isLiked = likedMovies.find(e => e.id === parseInt(id));

        if(isLiked) this.setState({liked: true});
        else this.setState({liked: false});
    }
}