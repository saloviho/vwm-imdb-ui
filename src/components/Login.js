import React from 'react'
import axios from 'axios';
import Cookies from 'universal-cookie';

export class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            username: "",
            password: "",
            logged: false
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render()
    {
        return (
            <>
                <h1>Login page</h1>
                {(this.state.error) ? <p className="text-danger">{this.state.error}</p>: ""}
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>Username</label>
                        <input name="username" className="form-control" onChange={this.handleInputChange}/>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input name="password" type="password" className="form-control" onChange={this.handleInputChange}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Login</button>
                </form>
            </>
        )
    }

    async handleSubmit(event) {
        event.preventDefault();

        const data = {
            username: this.state.username,
            password: this.state.password
        };

        try {
            const response = await axios.post("https://api.vwm-imdb.ml/login", data);
            const apiKey = response.data["user"]["apiKey"];

            const cookies = new Cookies();
            cookies.set('session', apiKey, { path: '/'});
            this.props.history.push('/');
            window.location.reload();
        } catch (error) {
            if (error.response) {
                this.setState({error: "Wrong username/password"});
            } else if (error.request) {
                this.setState({error: "Connection error"});
            } else {
                this.setState({error: "Unknown error. Try again later"});
            }
            console.log(error);
        }
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
}