import React from 'react'
import Cookies from 'universal-cookie';
import {Redirect} from "react-router-dom";

export class Logout extends React.Component {
    render()
    {
        const cookies = new Cookies();
        cookies.remove('session', { path: '/' });
        window.location.reload();
        return (
            <Redirect to={'/login'}/>
        )
    }
}