import React from 'react'
import {Director} from "./Director";

export class DirectorList extends React.Component {
    render() {
        return (
            <>
                <div className="movie-list-wrapper">
                {(this.props.list.length > 0)
                    ?
                    this.props.list.map((elem) =>
                        <Director key={elem.id} id={elem.id} name={elem.name}/>
                    )
                    :
                    <h5>Nothing to show...</h5>
                }
                </div>
            </>
        );
    }
}
