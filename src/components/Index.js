import React from 'react'
import {MovieList} from "./MovieList";
import axios from 'axios';
import {Link} from "react-router-dom";
import Cookies from 'universal-cookie';

export class Index extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {movies: []};
    }

    async componentDidMount()
    {
        await this.parseQueryParams();
    }

    async parseQueryParams()
    {
        let param = {};
        const queryString = this.props.location.search;
        queryString.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
            param[key] = value;
        });

        await this.getMovies(param["top"], param["offset"], param["year"], param["title"], param["type"]);
    }

    render() {
        if(this.state.movies && this.state.params)
        {
            const data = this.state.params;
            const prev = (parseInt(data.offset) - parseInt(data.top) < 0) ? 0 : parseInt(data.offset) - parseInt(data.top);
            const next = parseInt(data.offset) + parseInt(data.top);
            return (
                    <>
                        <MovieList list={this.state.movies}/>
                        <Link className="btn btn-primary page-button" to="" onClick={() => this.getMovies(data.top, prev, data.year, data.title, data.type)}>Prev</Link>
                        <Link className="btn btn-primary page-button mb-20" to="" onClick={() => this.getMovies(data.top, next, data.year, data.title, data.type)}>Next</Link>
                    </>
            );
        }else{
            return <></>;
        }
    }


    async getMovies(top, offset, year, title, type)
    {
        const cookies = new Cookies();
        const token = cookies.get("session");

        const param = {top: (top ? top : 10), offset: (offset ? offset : 0), year: year, title: title, type: type};
        const url = `https://api.vwm-imdb.ml/movie?top=${param["top"]}&offset=${param["offset"]}` + (year ?`&year=${year}`:'') + (title ?`&title=${title}`:'') + (type ?`&type=${type}`:'');
        const response = await axios.get(url, {
            headers: { Authorization: `Basic ${token}`}
        });

        const moviesList = response.data["movies"];
        this.setState({movies: moviesList, params: param});
        window.scrollTo(0, 0);
    }
}
