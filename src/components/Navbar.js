import React from 'react'
import {NavLink, Route} from "react-router-dom";

export class Navbar extends React.Component {

    render()
    {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light header mb-20">
                <NavLink to="/" className="navbar-brand">BI-VWM</NavLink>
                <button className="navbar-toggler" type="button">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav">
                        {this.props.isLogged ?
                            <>
                                <NavLink to="/directors" className="nav-item nav-link">Directors</NavLink>
                                <NavLink to="/liked" className="nav-item nav-link">Liked</NavLink>
                                <NavLink to="/recommend" className="nav-item nav-link">Recommended</NavLink>
                                <NavLink to="/logout" className="nav-item nav-link">Logout</NavLink>
                            </>
                            :
                            <>
                                <NavLink to="/login" className="nav-item nav-link">Login</NavLink>
                                <NavLink to="/register" className="nav-item nav-link">Register</NavLink>
                            </>
                        }
                    </div>
                </div>

                {this.props.isLogged ?
                    <>
                    <Route path="/" exact render={() => {
                        return (<form action="/">
                            <div className="form-row">
                                <div className="col-5">
                                    <input name="title" type="text" className="form-control" placeholder="Title"/>
                                </div>
                                <div className="col-2">
                                    <input name="year" type="text" className="form-control" placeholder="Year"/>
                                </div>

                                <div className="col-3">
                                    <select defaultValue="10" name="top" className="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                        <option value="10">10</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>

                                <div className="col">
                                    <button type="submit" className="btn btn-primary">Search</button>
                                </div>
                            </div>
                        </form>);
                    }}/>
                    <Route path="/directors" exact render={() => {
                        return(
                            <form action="/directors">
                                <div className="form-row">
                                    <div className="col-5">
                                        <input name="name" type="text" className="form-control" placeholder="Name"/>
                                    </div>

                                    <div className="col-3">
                                        <select defaultValue="10" name="top" className="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>

                                    <div className="col">
                                        <button type="submit" className="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </form>);
                    }}/>
                    </>
                    : <></>}
            </nav>
        )
    }
}