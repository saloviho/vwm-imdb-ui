import React from 'react'
import axios from 'axios';

export class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            username: "",
            password: "",
            passwordConfirm: ""
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render()
    {
        return (
            <>
                <h1>Register page</h1>
                {(this.state.error) ? <p className="text-danger">{this.state.error}</p>: ""}
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>First name</label>
                        <input name="firstName" className="form-control" onChange={this.handleInputChange}/>
                    </div>
                    <div className="form-group">
                        <label>Last name</label>
                        <input name="lastName" className="form-control" onChange={this.handleInputChange}/>
                    </div>
                    <div className="form-group">
                        <label>Username</label>
                        <input name="username" className="form-control" onChange={this.handleInputChange}/>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input name="password" type="password" className="form-control" onChange={this.handleInputChange}/>
                    </div>
                    <div className="form-group">
                        <label>Confirm password</label>
                        <input name="passwordConfirm" type="password" className="form-control" onChange={this.handleInputChange}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Register</button>
                </form>
            </>
        )
    }

    async handleSubmit(event) {
        event.preventDefault();

        const data = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            username: this.state.username,
            password: this.state.password,
            passwordConfirm: this.state.passwordConfirm
        };

        try {
            await axios.post("https://api.vwm-imdb.ml/register", data);
            this.props.history.push('/login');
        } catch (error) {
            if (error.response) {
                this.setState({error: "User already exists"});
            } else if (error.request) {
                this.setState({error: "Connection error"});
            } else {
                this.setState({error: "Unknown error. Try again later"});
            }
            console.log(error);
        }
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
}