import React from 'react'
import axios from 'axios';
import Cookies from 'universal-cookie';
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import {Navbar} from "./components/Navbar";
import {Recommend} from "./components/Recommend";
import {DirectorDetail} from "./components/DirectorDetail";
import {MovieDetail} from "./components/MovieDetail";
import {Register} from "./components/Register";
import {Login} from "./components/Login";
import {Index} from "./components/Index.js";
import {Logout} from "./components/Logout";
import {Liked} from "./components/Liked";
import {Directors} from "./components/Directors";

const ProtectedRoute
    = ({ isLogged, ...props }) =>
    isLogged
        ? <Route {...props}/>
        : <Redirect to="/login"/>;

const AuthRoute
    = ({ isLogged, ...props }) =>
    isLogged
        ? <Redirect to="/"/>
        : <Route {...props}/>;


export class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {access: undefined};
    }

    render()
    {
        if(this.state.access !== undefined)
        {
            const pr = this.state.access !== "denied";
            return (
                <>
                    <BrowserRouter>
                        <Navbar isLogged={pr}/>
                        <div className="page-grid">
                            <div className="center-wrapper">
                                <Switch>
                                    <ProtectedRoute isLogged={pr} path={'/'} exact component={Index}/>
                                    <AuthRoute isLogged={pr} path={'/login'} exact component={Login}/>
                                    <AuthRoute isLogged={pr} path={'/register'} exact component={Register}/>
                                    <ProtectedRoute isLogged={pr} path={'/movie/:id'} exact component={MovieDetail}/>
                                    <ProtectedRoute isLogged={pr} path={'/director/:id'} exact component={DirectorDetail}/>
                                    <ProtectedRoute isLogged={pr} path={'/recommend'} exact component={Recommend}/>
                                    <ProtectedRoute isLogged={pr} path={'/directors'} exact component={Directors}/>
                                    <ProtectedRoute isLogged={pr} path={'/liked'} exact component={Liked}/>
                                    <ProtectedRoute isLogged={pr} path={'/logout'} exact component={Logout}/>
                                </Switch>
                            </div>
                        </div>
                    </BrowserRouter>
                </>
            )
        }else{
            return <></>;
        }

    }

    async componentDidMount() {
        await this.auth();
    }

    async auth()
    {
        console.log("AUTH");
        const cookies = new Cookies();
        const session = cookies.get("session");
        if(!session) this.setState({access: "denied"});
        else{
            const response = await axios.get('https://api.vwm-imdb.ml/me', {
                headers: { Authorization: `Basic ${session}`}
            });

            if(response.status !== 200) this.setState({access: "denied"});
            else this.setState({access: "granted"});
        }
    }
}