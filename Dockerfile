FROM node:12
RUN mkdir -p /var/ui
ADD . /var/ui
WORKDIR "/var/ui"
RUN npm install
RUN npm run build
EXPOSE 3002
CMD ["npm", "run", "serve"]
